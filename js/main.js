$(document).ready(function(){
    $('.header').height($(window).height()); // potrzebne do tła
    
    $('.nav-item').first().addClass('active');
    
    /* scroll */
    $('.navbar a').click(function(){
  	var navbarHeight = parseInt($('.navbar').css('height'), 10);
 	$('body,html').animate({
 		scrollTop:$('#' + $(this).data('value')).offset().top-navbarHeight
 	},1000)
  
    })
    
    /* active nav-item */
    var selectorNav = '.nav-item';
    $(selectorNav).on('click', function(){
        $(selectorNav).removeClass('active');
        $(this).addClass('active');
    });
    
    /* active portfolio buttons*/
    var selectorBtn = 'button';
    $(selectorBtn).on('click', function(){
        $(selectorBtn).removeClass('active');
        $(this).addClass('active');
    });
    
})

/* portfolio */
var selectedClass = '';
$('.filter').click(function(){
    selectedClass = $(this).attr('data-rel');
    $('#gallery').fadeTo(200, 0.1);
    $('#gallery div div').not('.'+selectedClass).fadeOut().removeClass('animation');
    setTimeout(function() {
        $('.'+selectedClass).fadeIn().addClass('animation');
        $('#gallery').fadeTo(300, 1);
        }, 300);
});

/* contact form, validation */
window.addEventListener('load', function() {
    var forms = document.getElementsByClassName('needs-validation');
    var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
        
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
            console.log('bad validation');
            form.classList.add('was-validated');
        } else {
          event.preventDefault();
          var url = 'contact.php';

            // POST values in the background the the script URL
            $.ajax({
                type: 'POST',
                url: url,
                data: $(this).serialize(),
                success: function (data)
                {
                    // data = JSON object that contact.php returns
                    // we recieve the type of the message: success x danger and apply it to the 
                    var messageAlert = 'alert-' + data.type;
                    var messageText = data.message;

                    // let's compose Bootstrap alert box HTML
                    var alertBox = '<div class="alert ' + messageAlert + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + messageText + '</div>';
                    
                    if (messageAlert && messageText) {
                        $('#contact-form').find('.messages').html(alertBox);
                        //$('#contact-form')[0].reset();
                        //$('#msg').val('').change();
                        form.classList.add('was-validated');
                        //$('#contact-form').addClass('was-validated');
                    }
                }
            });
        }

        
      }, false);
   });
}, false);

