<?php
require_once("config.php");

$from = 'Jan Nowak <jan@nowak.pl>';

$sendTo = 'Jan Nowak <jan@nowak.pl>';

$subject = 'Wiadomość z strony';

$fields = array('flname' => 'Name', 'email' => 'Email', 'msg' => 'Message'); 

$okMessage = 'Wiadomość została pomyślnie wysłana.';

$errorMessage = 'Wystąpił błąd podczas wysyłania wiadomości. Spróbuj ponownie później';


try
{

    if(count($_POST) == 0) throw new \Exception('Formularz jest pusty');
        
    /*
        Struktura bazy danych:
        CREATE TABLE contact_form (
                                  ID int(11) AUTO_INCREMENT,
                                  FLNAME TINYTEXT NOT NULL,
                                  EMAIL TINYTEXT NOT NULL,
						          MSG TEXT NOT NULL,
                                  PRIMARY KEY  (ID)
                                  );
    */

    $yourName = $conn->real_escape_string($_POST['flname']);
    $yourEmail = $conn->real_escape_string($_POST['email']);
    $msg = $conn->real_escape_string($_POST['msg']);

    $sql="INSERT INTO contact_form (FLNAME, EMAIL, MSG) VALUES ('".$yourName."','".$yourEmail."', '".$msg."')";

    $result = $conn->query($sql);

    if (!$result) {
        printf("Error: [%s] %s<br/>", $conn->errno, $conn->error);
    }

    $conn->close();
         
    $emailText = "Otrzymałeś wiadomość z strony.\n=============================\n";

    foreach ($_POST as $key => $value) {
        if (isset($fields[$key])) {
            $emailText .= "$fields[$key]: $value\n";
        }
    }

    $headers = array('Content-Type: text/plain; charset="UTF-8";',
        'From: ' . $from,
        'Reply-To: ' . $from,
        'Return-Path: ' . $from,
    );
    
    // wyslij maila
    mail($sendTo, $subject, $emailText, implode("\n", $headers));

    $responseArray = array('type' => 'success', 'message' => $okMessage);
    

    
}
catch (\Exception $e)
{
    $responseArray = array('type' => 'danger', 'message' => $errorMessage);
}

// zwrotna informacja
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $encoded = json_encode($responseArray);

    header('Content-Type: application/json');

    echo $encoded;
}
else {
    echo $responseArray['message'];
}
?>
